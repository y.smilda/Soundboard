<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.2.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.05" unitdist="inch" unit="inch" style="dots" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="16" fill="1" visible="no" active="no"/>
<layer number="3" name="Route3" color="17" fill="1" visible="no" active="no"/>
<layer number="4" name="Route4" color="18" fill="1" visible="no" active="no"/>
<layer number="5" name="Route5" color="19" fill="1" visible="no" active="no"/>
<layer number="6" name="Route6" color="25" fill="1" visible="no" active="no"/>
<layer number="7" name="Route7" color="26" fill="1" visible="no" active="no"/>
<layer number="8" name="Route8" color="27" fill="1" visible="no" active="no"/>
<layer number="9" name="Route9" color="28" fill="1" visible="no" active="no"/>
<layer number="10" name="Route10" color="29" fill="1" visible="no" active="no"/>
<layer number="11" name="Route11" color="30" fill="1" visible="no" active="no"/>
<layer number="12" name="Route12" color="20" fill="1" visible="no" active="no"/>
<layer number="13" name="Route13" color="21" fill="1" visible="no" active="no"/>
<layer number="14" name="Route14" color="22" fill="1" visible="no" active="no"/>
<layer number="15" name="Route15" color="23" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="Motherboard">
<packages>
<package name="MODULE_ESP-WROOM-32">
<wire x1="-9" y1="-9.755" x2="9" y2="-9.755" width="0.127" layer="51"/>
<wire x1="9" y1="-9.755" x2="9" y2="15.745" width="0.127" layer="51"/>
<wire x1="9" y1="15.745" x2="-9" y2="15.745" width="0.127" layer="51"/>
<wire x1="-9" y1="15.745" x2="-9" y2="-9.755" width="0.127" layer="51"/>
<wire x1="-9" y1="-9" x2="-9" y2="-9.75" width="0.127" layer="21"/>
<wire x1="-9" y1="-9.75" x2="-6.5" y2="-9.75" width="0.127" layer="21"/>
<wire x1="6.5" y1="-9.75" x2="9" y2="-9.75" width="0.127" layer="21"/>
<wire x1="9" y1="-9.75" x2="9" y2="-9" width="0.127" layer="21"/>
<wire x1="-9" y1="9.25" x2="-9" y2="15.75" width="0.127" layer="21"/>
<wire x1="-9" y1="15.75" x2="9" y2="15.75" width="0.127" layer="21"/>
<wire x1="9" y1="15.75" x2="9" y2="9.25" width="0.127" layer="21"/>
<rectangle x1="-8.02203125" y1="10.5289" x2="-7.5" y2="15" layer="21"/>
<rectangle x1="-8.009109375" y1="14.5165" x2="-5" y2="15" layer="21"/>
<rectangle x1="-5.52168125" y1="12.0474" x2="-5" y2="14.5" layer="21"/>
<rectangle x1="-5.508290625" y1="12.0181" x2="-2.5" y2="12.5" layer="21"/>
<rectangle x1="-3.01023125" y1="12.0408" x2="-2.5" y2="14.5" layer="21"/>
<rectangle x1="-3.01011875" y1="14.5488" x2="0" y2="15" layer="21"/>
<rectangle x1="-0.502034375" y1="12.0488" x2="0" y2="14.5" layer="21"/>
<rectangle x1="-0.5020875" y1="12.0501" x2="2.5" y2="12.5" layer="21"/>
<rectangle x1="2.0088" y1="12.0528" x2="2.5" y2="14.5" layer="21"/>
<rectangle x1="2.01156875" y1="14.5137" x2="7.5" y2="15" layer="21"/>
<rectangle x1="7.0207" y1="10.0296" x2="7.5" y2="15" layer="21"/>
<rectangle x1="4.513959375" y1="10.0311" x2="5" y2="15" layer="21"/>
<rectangle x1="7.01886875" y1="9.525609375" x2="7.5" y2="10" layer="21"/>
<rectangle x1="4.508090625" y1="9.51706875" x2="5" y2="10" layer="21"/>
<rectangle x1="-8.02033125" y1="10.0254" x2="-7.5" y2="10.5" layer="21"/>
<rectangle x1="2.007940625" y1="13.5536" x2="2.5" y2="15" layer="21"/>
<rectangle x1="-8.03838125" y1="10.5504" x2="-7.5" y2="15" layer="51"/>
<rectangle x1="-8.04086875" y1="14.5741" x2="-5" y2="15" layer="51"/>
<rectangle x1="-5.51816875" y1="12.0396" x2="-5" y2="14.5" layer="51"/>
<rectangle x1="-5.5155" y1="12.0339" x2="-2.5" y2="12.5" layer="51"/>
<rectangle x1="-3.01036875" y1="12.0415" x2="-2.5" y2="14.5" layer="51"/>
<rectangle x1="-3.0064" y1="14.531" x2="0" y2="15" layer="51"/>
<rectangle x1="-0.501671875" y1="12.0402" x2="0" y2="14.5" layer="51"/>
<rectangle x1="-0.500515625" y1="12.0123" x2="2.5" y2="12.5" layer="51"/>
<rectangle x1="2.007690625" y1="12.0461" x2="2.5" y2="14.5" layer="51"/>
<rectangle x1="2.015290625" y1="14.5407" x2="7.5" y2="15" layer="51"/>
<rectangle x1="7.0261" y1="10.0373" x2="7.5" y2="15" layer="51"/>
<rectangle x1="4.514609375" y1="10.0326" x2="5" y2="15" layer="51"/>
<rectangle x1="7.02153125" y1="9.52923125" x2="7.5" y2="10" layer="51"/>
<rectangle x1="4.522840625" y1="9.548240625" x2="5" y2="10" layer="51"/>
<rectangle x1="-8.02665" y1="10.0334" x2="-7.5" y2="10.5" layer="51"/>
<rectangle x1="2.007590625" y1="13.5513" x2="2.5" y2="15" layer="51"/>
<wire x1="-9.25" y1="16" x2="9.25" y2="16" width="0.05" layer="39"/>
<wire x1="9.25" y1="16" x2="9.25" y2="9" width="0.05" layer="39"/>
<wire x1="9.25" y1="9" x2="10.1" y2="9" width="0.05" layer="39"/>
<wire x1="10.1" y1="9" x2="10.1" y2="-9" width="0.05" layer="39"/>
<wire x1="10.1" y1="-9" x2="9.25" y2="-9" width="0.05" layer="39"/>
<wire x1="9.25" y1="-9" x2="9.25" y2="-10" width="0.05" layer="39"/>
<wire x1="9.25" y1="-10" x2="6.5" y2="-10" width="0.05" layer="39"/>
<wire x1="6.5" y1="-10" x2="6.5" y2="-10.855" width="0.05" layer="39"/>
<wire x1="6.5" y1="-10.855" x2="-6.5" y2="-10.855" width="0.05" layer="39"/>
<wire x1="-6.5" y1="-10.855" x2="-6.5" y2="-10" width="0.05" layer="39"/>
<wire x1="-6.5" y1="-10" x2="-9.25" y2="-10" width="0.05" layer="39"/>
<wire x1="-9.25" y1="-10" x2="-9.25" y2="-9" width="0.05" layer="39"/>
<wire x1="-9.25" y1="-9" x2="-10.1" y2="-9" width="0.05" layer="39"/>
<wire x1="-10.1" y1="-9" x2="-10.1" y2="9" width="0.05" layer="39"/>
<wire x1="-10.1" y1="9" x2="-9.25" y2="9" width="0.05" layer="39"/>
<wire x1="-9.25" y1="9" x2="-9.25" y2="16" width="0.05" layer="39"/>
<text x="-9.03255" y="16.3088" size="1.68246875" layer="25">&gt;NAME</text>
<text x="-9.037009375" y="-12.5515" size="1.683309375" layer="27">&gt;VALUE</text>
<rectangle x1="-9.02585" y1="9.02585" x2="9" y2="15.75" layer="41"/>
<rectangle x1="-9.01176875" y1="9.01176875" x2="9" y2="15.75" layer="42"/>
<rectangle x1="-9.024490625" y1="9.024490625" x2="9" y2="15.75" layer="43"/>
<circle x="-10.365" y="8.2754" radius="0.1" width="0.2" layer="21"/>
<circle x="-7.425" y="8.2754" radius="0.1" width="0.2" layer="51"/>
<smd name="38" x="9" y="8.255" dx="1.7" dy="0.9" layer="1" roundness="22" rot="R180"/>
<smd name="37" x="9" y="6.985" dx="1.7" dy="0.9" layer="1" roundness="22" rot="R180"/>
<smd name="36" x="9" y="5.715" dx="1.7" dy="0.9" layer="1" roundness="22" rot="R180"/>
<smd name="35" x="9" y="4.445" dx="1.7" dy="0.9" layer="1" roundness="22" rot="R180"/>
<smd name="34" x="9" y="3.175" dx="1.7" dy="0.9" layer="1" roundness="22" rot="R180"/>
<smd name="33" x="9" y="1.905" dx="1.7" dy="0.9" layer="1" roundness="22" rot="R180"/>
<smd name="32" x="9" y="0.635" dx="1.7" dy="0.9" layer="1" roundness="22" rot="R180"/>
<smd name="31" x="9" y="-0.635" dx="1.7" dy="0.9" layer="1" roundness="22" rot="R180"/>
<smd name="30" x="9" y="-1.905" dx="1.7" dy="0.9" layer="1" roundness="22" rot="R180"/>
<smd name="29" x="9" y="-3.175" dx="1.7" dy="0.9" layer="1" roundness="22" rot="R180"/>
<smd name="28" x="9" y="-4.445" dx="1.7" dy="0.9" layer="1" roundness="22" rot="R180"/>
<smd name="27" x="9" y="-5.715" dx="1.7" dy="0.9" layer="1" roundness="22" rot="R180"/>
<smd name="26" x="9" y="-6.985" dx="1.7" dy="0.9" layer="1" roundness="22" rot="R180"/>
<smd name="25" x="9" y="-8.255" dx="1.7" dy="0.9" layer="1" roundness="22" rot="R180"/>
<smd name="24" x="5.715" y="-9.755" dx="1.7" dy="0.9" layer="1" roundness="22" rot="R90"/>
<smd name="23" x="4.445" y="-9.755" dx="1.7" dy="0.9" layer="1" roundness="22" rot="R90"/>
<smd name="22" x="3.175" y="-9.755" dx="1.7" dy="0.9" layer="1" roundness="22" rot="R90"/>
<smd name="21" x="1.905" y="-9.755" dx="1.7" dy="0.9" layer="1" roundness="22" rot="R90"/>
<smd name="20" x="0.635" y="-9.755" dx="1.7" dy="0.9" layer="1" roundness="22" rot="R90"/>
<smd name="19" x="-0.635" y="-9.755" dx="1.7" dy="0.9" layer="1" roundness="22" rot="R90"/>
<smd name="18" x="-1.905" y="-9.755" dx="1.7" dy="0.9" layer="1" roundness="22" rot="R90"/>
<smd name="17" x="-3.175" y="-9.755" dx="1.7" dy="0.9" layer="1" roundness="22" rot="R90"/>
<smd name="16" x="-4.445" y="-9.755" dx="1.7" dy="0.9" layer="1" roundness="22" rot="R90"/>
<smd name="15" x="-5.715" y="-9.755" dx="1.7" dy="0.9" layer="1" roundness="22" rot="R90"/>
<smd name="14" x="-9" y="-8.255" dx="1.7" dy="0.9" layer="1" roundness="22"/>
<smd name="13" x="-9" y="-6.985" dx="1.7" dy="0.9" layer="1" roundness="22"/>
<smd name="12" x="-9" y="-5.715" dx="1.7" dy="0.9" layer="1" roundness="22"/>
<smd name="11" x="-9" y="-4.445" dx="1.7" dy="0.9" layer="1" roundness="22"/>
<smd name="10" x="-9" y="-3.175" dx="1.7" dy="0.9" layer="1" roundness="22"/>
<smd name="9" x="-9" y="-1.905" dx="1.7" dy="0.9" layer="1" roundness="22"/>
<smd name="8" x="-9" y="-0.635" dx="1.7" dy="0.9" layer="1" roundness="22"/>
<smd name="7" x="-9" y="0.635" dx="1.7" dy="0.9" layer="1" roundness="22"/>
<smd name="6" x="-9" y="1.905" dx="1.7" dy="0.9" layer="1" roundness="22"/>
<smd name="5" x="-9" y="3.175" dx="1.7" dy="0.9" layer="1" roundness="22"/>
<smd name="4" x="-9" y="4.445" dx="1.7" dy="0.9" layer="1" roundness="22"/>
<smd name="3" x="-9" y="5.715" dx="1.7" dy="0.9" layer="1" roundness="22"/>
<smd name="2" x="-9" y="6.985" dx="1.7" dy="0.9" layer="1" roundness="22"/>
<smd name="1" x="-9" y="8.255" dx="1.7" dy="0.9" layer="1" roundness="22"/>
<smd name="1.1" x="-0.7" y="0.845" dx="4" dy="4" layer="1" rot="R180"/>
</package>
<package name="CHERRY-MX-RGB">
<description>Cherry MX Keyswitch footprint</description>
<wire x1="-7.62" y1="7.62" x2="7.62" y2="7.62" width="0.127" layer="21"/>
<wire x1="7.62" y1="7.62" x2="7.62" y2="-7.62" width="0.127" layer="21"/>
<wire x1="7.62" y1="-7.62" x2="-7.62" y2="-7.62" width="0.127" layer="21"/>
<wire x1="-7.62" y1="-7.62" x2="-7.62" y2="7.62" width="0.127" layer="21"/>
<pad name="SW1" x="-3.81" y="2.54" drill="1.5" diameter="2.54"/>
<pad name="SW2" x="2.54" y="5.08" drill="1.5" diameter="2.54"/>
<pad name="P$3" x="-5.08" y="0" drill="1.7144" diameter="1.9304"/>
<pad name="P$4" x="5.08" y="0" drill="1.7144" diameter="1.9304"/>
<hole x="0" y="0" drill="4.0004"/>
<smd name="VDD" x="-3.35" y="-3.19" dx="1.5" dy="1" layer="16"/>
<smd name="DOUT" x="-3.35" y="-6.39" dx="1.5" dy="1" layer="16"/>
<smd name="DIN" x="3.25" y="-3.19" dx="1.5" dy="1" layer="16"/>
<smd name="VSS" x="3.25" y="-6.39" dx="1.5" dy="1" layer="16"/>
<wire x1="-2.55" y1="-2.29" x2="2.45" y2="-2.29" width="0.127" layer="21"/>
<wire x1="2.45" y1="-2.29" x2="2.45" y2="-7.29" width="0.127" layer="21"/>
<wire x1="2.45" y1="-7.29" x2="-2.55" y2="-7.29" width="0.127" layer="21"/>
<wire x1="-2.55" y1="-7.29" x2="-2.55" y2="-2.29" width="0.127" layer="21"/>
<wire x1="-2.55" y1="-2.29" x2="2.45" y2="-2.29" width="0.127" layer="20"/>
<wire x1="2.45" y1="-2.29" x2="2.45" y2="-7.29" width="0.127" layer="20"/>
<wire x1="2.45" y1="-7.29" x2="-2.55" y2="-7.29" width="0.127" layer="20"/>
<wire x1="-2.55" y1="-7.29" x2="-2.55" y2="-2.29" width="0.127" layer="20"/>
</package>
<package name="D0402" urn="urn:adsk.eagle:footprint:23043/3" locally_modified="yes">
<description>&lt;b&gt;Chip RESISTOR 0402 EIA (1005 Metric)&lt;/b&gt;</description>
<wire x1="-1" y1="0.483" x2="1" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1" y1="0.483" x2="1" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1" y1="-0.483" x2="-1" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1" y1="-0.483" x2="-1" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.5" y="0" dx="0.6" dy="0.7" layer="1"/>
<smd name="2" x="0.5" y="0" dx="0.6" dy="0.7" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.1999" y1="-0.35" x2="0.1999" y2="0.35" layer="35"/>
<wire x1="-0.2" y1="0.2" x2="-0.2" y2="-0.2" width="0.127" layer="21"/>
<wire x1="-0.2" y1="-0.2" x2="0.2" y2="0" width="0.127" layer="21"/>
<wire x1="0.2" y1="0" x2="-0.2" y2="0.2" width="0.127" layer="21"/>
<wire x1="0.2" y1="0.2" x2="0.2" y2="-0.2" width="0.127" layer="21"/>
<wire x1="-0.1" y1="0.1" x2="-0.1" y2="-0.1" width="0.127" layer="21"/>
<wire x1="0.1" y1="0" x2="0" y2="0" width="0.127" layer="21"/>
</package>
<package name="D0603" urn="urn:adsk.eagle:footprint:23044/1" locally_modified="yes">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.85" y="0" dx="1" dy="1.1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1" dy="1.1" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
<polygon width="0.127" layer="21">
<vertex x="-0.3" y="-0.4"/>
<vertex x="-0.3" y="0.4"/>
<vertex x="0.3" y="0"/>
</polygon>
<wire x1="0.3" y1="0.4" x2="0.3" y2="-0.4" width="0.127" layer="21"/>
</package>
<package name="D0805" urn="urn:adsk.eagle:footprint:23045/1" locally_modified="yes">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
<polygon width="0.127" layer="21">
<vertex x="-0.3" y="0.3"/>
<vertex x="-0.3" y="-0.3"/>
<vertex x="0.3" y="0"/>
</polygon>
<wire x1="0.3" y1="0.3" x2="0.3" y2="-0.3" width="0.127" layer="21"/>
</package>
<package name="D1206" urn="urn:adsk.eagle:footprint:23047/1" locally_modified="yes">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="2" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="1" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
<polygon width="0.127" layer="21">
<vertex x="-0.5" y="0.3"/>
<vertex x="-0.5" y="-0.6"/>
<vertex x="0.4" y="0"/>
<vertex x="-0.5" y="0.6"/>
</polygon>
<wire x1="0.4" y1="0.6" x2="0.4" y2="-0.6" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="ESP-WROOM-32">
<wire x1="-15.24" y1="-27.94" x2="15.24" y2="-27.94" width="0.254" layer="94"/>
<wire x1="15.24" y1="-27.94" x2="15.24" y2="27.94" width="0.254" layer="94"/>
<wire x1="15.24" y1="27.94" x2="-15.24" y2="27.94" width="0.254" layer="94"/>
<wire x1="-15.24" y1="27.94" x2="-15.24" y2="-27.94" width="0.254" layer="94"/>
<text x="-15.2699" y="28.5037" size="2.54498125" layer="95">&gt;NAME</text>
<text x="-15.2772" y="-31.0638" size="2.5462" layer="96">&gt;VALUE</text>
<pin name="GND" x="20.32" y="-25.4" length="middle" direction="pwr" rot="R180"/>
<pin name="3V3" x="20.32" y="25.4" length="middle" direction="pwr" rot="R180"/>
<pin name="EN" x="-20.32" y="20.32" length="middle" direction="in"/>
<pin name="SENSOR_VP" x="-20.32" y="17.78" length="middle" direction="in"/>
<pin name="SENSOR_VN" x="-20.32" y="15.24" length="middle" direction="in"/>
<pin name="IO34" x="20.32" y="-17.78" length="middle" rot="R180"/>
<pin name="IO35" x="20.32" y="-20.32" length="middle" rot="R180"/>
<pin name="TXD0" x="-20.32" y="12.7" length="middle"/>
<pin name="RXD0" x="-20.32" y="10.16" length="middle"/>
<pin name="SWP/SD3" x="-20.32" y="-5.08" length="middle"/>
<pin name="SHD/SD2" x="-20.32" y="-2.54" length="middle"/>
<pin name="SCS/CMD" x="-20.32" y="7.62" length="middle"/>
<pin name="SCK/CLK" x="-20.32" y="5.08" length="middle" function="clk"/>
<pin name="SDO/SD0" x="-20.32" y="2.54" length="middle"/>
<pin name="SDI/SD1" x="-20.32" y="0" length="middle"/>
<pin name="IO0" x="-20.32" y="-10.16" length="middle"/>
<pin name="IO2" x="-20.32" y="-12.7" length="middle"/>
<pin name="IO4" x="-20.32" y="-15.24" length="middle"/>
<pin name="IO5" x="-20.32" y="-17.78" length="middle"/>
<pin name="IO12" x="-20.32" y="-20.32" length="middle"/>
<pin name="IO13" x="20.32" y="20.32" length="middle" rot="R180"/>
<pin name="IO14" x="20.32" y="17.78" length="middle" rot="R180"/>
<pin name="IO15" x="20.32" y="15.24" length="middle" rot="R180"/>
<pin name="IO16" x="20.32" y="12.7" length="middle" rot="R180"/>
<pin name="IO17" x="20.32" y="10.16" length="middle" rot="R180"/>
<pin name="IO18" x="20.32" y="7.62" length="middle" rot="R180"/>
<pin name="IO19" x="20.32" y="5.08" length="middle" rot="R180"/>
<pin name="IO21" x="20.32" y="2.54" length="middle" rot="R180"/>
<pin name="IO22" x="20.32" y="0" length="middle" rot="R180"/>
<pin name="IO23" x="20.32" y="-2.54" length="middle" rot="R180"/>
<pin name="IO25" x="20.32" y="-5.08" length="middle" rot="R180"/>
<pin name="IO26" x="20.32" y="-7.62" length="middle" rot="R180"/>
<pin name="IO27" x="20.32" y="-10.16" length="middle" rot="R180"/>
<pin name="IO32" x="20.32" y="-12.7" length="middle" rot="R180"/>
<pin name="IO33" x="20.32" y="-15.24" length="middle" rot="R180"/>
</symbol>
<symbol name="CHERRY-MX">
<description>Basic part for Cherry MX series keyswitch</description>
<text x="-6.096" y="5.842" size="1.4224" layer="95">CHERRY-MX</text>
<pin name="PIN-1" x="-10.16" y="2.54" length="middle"/>
<pin name="PIN-2" x="-10.16" y="-2.54" length="middle"/>
<text x="-5.08" y="-7.62" size="1.778" layer="95">&gt;NAME</text>
<wire x1="-5.08" y1="5.08" x2="-5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-5.08" x2="5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="5.08" x2="-5.08" y2="5.08" width="0.254" layer="94"/>
</symbol>
<symbol name="SK6812">
<pin name="VDD" x="-2.54" y="0" visible="pad" length="short"/>
<pin name="DIN" x="-2.54" y="-5.08" visible="pad" length="short"/>
<pin name="VSS" x="6.985" y="0" visible="pad" length="short" rot="R180"/>
<pin name="DOUT" x="6.985" y="-5.08" visible="pad" length="short" rot="R180"/>
<wire x1="0" y1="0" x2="4.445" y2="0" width="0.254" layer="94"/>
<wire x1="4.445" y1="0" x2="4.445" y2="-5.08" width="0.254" layer="94"/>
<wire x1="4.445" y1="-5.08" x2="0" y2="-5.08" width="0.254" layer="94"/>
<wire x1="0" y1="-5.08" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="-1.905" x2="1.27" y2="-4.445" width="0.254" layer="94"/>
<wire x1="1.27" y1="-4.445" x2="3.175" y2="-3.175" width="0.254" layer="94"/>
<wire x1="3.175" y1="-3.175" x2="1.27" y2="-1.905" width="0.254" layer="94"/>
<wire x1="3.175" y1="-1.905" x2="3.175" y2="-4.445" width="0.254" layer="94"/>
<wire x1="1.905" y1="-1.5875" x2="2.2225" y2="-0.9525" width="0.254" layer="94"/>
<wire x1="2.54" y1="-1.5875" x2="2.8575" y2="-0.9525" width="0.254" layer="94"/>
</symbol>
<symbol name="+3V3">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+3V3" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="GND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="DIODE">
<wire x1="0" y1="1.27" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="0" y2="1.27" width="0.254" layer="94"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<pin name="P$1" x="-1.27" y="0" visible="pad" length="point"/>
<pin name="P$2" x="3.81" y="0" visible="pad" length="point"/>
<wire x1="-1.27" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="3.81" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ESP-WROOM-32" prefix="U">
<description>Module: combo; GPIO, I2C x2, I2S x2, SDIO, SPI x3, UART x3; 19.5dBm</description>
<gates>
<gate name="G$1" symbol="ESP-WROOM-32" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MODULE_ESP-WROOM-32">
<connects>
<connect gate="G$1" pin="3V3" pad="2"/>
<connect gate="G$1" pin="EN" pad="3"/>
<connect gate="G$1" pin="GND" pad="1 1.1 15 38"/>
<connect gate="G$1" pin="IO0" pad="25"/>
<connect gate="G$1" pin="IO12" pad="14"/>
<connect gate="G$1" pin="IO13" pad="16"/>
<connect gate="G$1" pin="IO14" pad="13"/>
<connect gate="G$1" pin="IO15" pad="23"/>
<connect gate="G$1" pin="IO16" pad="27"/>
<connect gate="G$1" pin="IO17" pad="28"/>
<connect gate="G$1" pin="IO18" pad="30"/>
<connect gate="G$1" pin="IO19" pad="31"/>
<connect gate="G$1" pin="IO2" pad="24"/>
<connect gate="G$1" pin="IO21" pad="33"/>
<connect gate="G$1" pin="IO22" pad="36"/>
<connect gate="G$1" pin="IO23" pad="37"/>
<connect gate="G$1" pin="IO25" pad="10"/>
<connect gate="G$1" pin="IO26" pad="11"/>
<connect gate="G$1" pin="IO27" pad="12"/>
<connect gate="G$1" pin="IO32" pad="8"/>
<connect gate="G$1" pin="IO33" pad="9"/>
<connect gate="G$1" pin="IO34" pad="6"/>
<connect gate="G$1" pin="IO35" pad="7"/>
<connect gate="G$1" pin="IO4" pad="26"/>
<connect gate="G$1" pin="IO5" pad="29"/>
<connect gate="G$1" pin="RXD0" pad="34"/>
<connect gate="G$1" pin="SCK/CLK" pad="20"/>
<connect gate="G$1" pin="SCS/CMD" pad="19"/>
<connect gate="G$1" pin="SDI/SD1" pad="22"/>
<connect gate="G$1" pin="SDO/SD0" pad="21"/>
<connect gate="G$1" pin="SENSOR_VN" pad="5"/>
<connect gate="G$1" pin="SENSOR_VP" pad="4"/>
<connect gate="G$1" pin="SHD/SD2" pad="17"/>
<connect gate="G$1" pin="SWP/SD3" pad="18"/>
<connect gate="G$1" pin="TXD0" pad="35"/>
</connects>
<technologies>
<technology name="">
<attribute name="AVAILABILITY" value="Unavailable"/>
<attribute name="DESCRIPTION" value=" Module: combo; GPIO, I2C x2, I2S x2, SDIO, SPI x3, UART x3; 19.5dBm "/>
<attribute name="MF" value="Olimex LTD"/>
<attribute name="MP" value="ESP-WROOM-32"/>
<attribute name="PACKAGE" value="Module Espressif Systems"/>
<attribute name="PRICE" value="None"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CHERRY-MX-RGB">
<description>Cherry MX series keyswitch</description>
<gates>
<gate name="G$1" symbol="CHERRY-MX" x="-2.54" y="-25.4"/>
<gate name="G$2" symbol="SK6812" x="-5.08" y="-38.1"/>
</gates>
<devices>
<device name="" package="CHERRY-MX-RGB">
<connects>
<connect gate="G$1" pin="PIN-1" pad="SW1"/>
<connect gate="G$1" pin="PIN-2" pad="SW2"/>
<connect gate="G$2" pin="DIN" pad="DIN"/>
<connect gate="G$2" pin="DOUT" pad="DOUT"/>
<connect gate="G$2" pin="VDD" pad="VDD"/>
<connect gate="G$2" pin="VSS" pad="VSS"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+3V3" prefix="+3V3">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="+3V3" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DIODE">
<gates>
<gate name="G$1" symbol="DIODE" x="0" y="0"/>
</gates>
<devices>
<device name="0402" package="D0402">
<connects>
<connect gate="G$1" pin="P$1" pad="1"/>
<connect gate="G$1" pin="P$2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="D0603">
<connects>
<connect gate="G$1" pin="P$1" pad="1"/>
<connect gate="G$1" pin="P$2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="D0805">
<connects>
<connect gate="G$1" pin="P$1" pad="1"/>
<connect gate="G$1" pin="P$2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="D1206">
<connects>
<connect gate="G$1" pin="P$1" pad="1"/>
<connect gate="G$1" pin="P$2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1" urn="urn:adsk.eagle:library:371">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="+3V3" urn="urn:adsk.eagle:symbol:26950/1" library_version="1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+3V3" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="GND" urn="urn:adsk.eagle:symbol:26925/1" library_version="1">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="+3V3" urn="urn:adsk.eagle:component:26981/1" prefix="+3V3" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="+3V3" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND" urn="urn:adsk.eagle:component:26954/1" prefix="GND" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="U1" library="Motherboard" deviceset="ESP-WROOM-32" device=""/>
<part name="U$1" library="Motherboard" deviceset="CHERRY-MX-RGB" device=""/>
<part name="U$2" library="Motherboard" deviceset="CHERRY-MX-RGB" device=""/>
<part name="U$3" library="Motherboard" deviceset="CHERRY-MX-RGB" device=""/>
<part name="U$4" library="Motherboard" deviceset="CHERRY-MX-RGB" device=""/>
<part name="U$5" library="Motherboard" deviceset="CHERRY-MX-RGB" device=""/>
<part name="U$6" library="Motherboard" deviceset="CHERRY-MX-RGB" device=""/>
<part name="U$7" library="Motherboard" deviceset="CHERRY-MX-RGB" device=""/>
<part name="U$8" library="Motherboard" deviceset="CHERRY-MX-RGB" device=""/>
<part name="U$9" library="Motherboard" deviceset="CHERRY-MX-RGB" device=""/>
<part name="U$10" library="Motherboard" deviceset="CHERRY-MX-RGB" device=""/>
<part name="U$11" library="Motherboard" deviceset="CHERRY-MX-RGB" device=""/>
<part name="U$12" library="Motherboard" deviceset="CHERRY-MX-RGB" device=""/>
<part name="U$13" library="Motherboard" deviceset="CHERRY-MX-RGB" device=""/>
<part name="U$14" library="Motherboard" deviceset="CHERRY-MX-RGB" device=""/>
<part name="U$15" library="Motherboard" deviceset="CHERRY-MX-RGB" device=""/>
<part name="U$16" library="Motherboard" deviceset="CHERRY-MX-RGB" device=""/>
<part name="+3V1" library="Motherboard" deviceset="+3V3" device=""/>
<part name="GND1" library="Motherboard" deviceset="GND" device=""/>
<part name="+3V2" library="Motherboard" deviceset="+3V3" device=""/>
<part name="+3V3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="+3V4" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="+3V5" library="Motherboard" deviceset="+3V3" device=""/>
<part name="GND2" library="Motherboard" deviceset="GND" device=""/>
<part name="GND3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND4" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND5" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="U$17" library="Motherboard" deviceset="DIODE" device="0805" value="TS4148 RYG"/>
<part name="U$18" library="Motherboard" deviceset="DIODE" device="0805" value="TS4148 RYG"/>
<part name="U$19" library="Motherboard" deviceset="DIODE" device="0805" value="TS4148 RYG"/>
<part name="U$20" library="Motherboard" deviceset="DIODE" device="0805" value="TS4148 RYG"/>
<part name="U$21" library="Motherboard" deviceset="DIODE" device="0805" value="TS4148 RYG"/>
<part name="U$22" library="Motherboard" deviceset="DIODE" device="0805" value="TS4148 RYG"/>
<part name="U$23" library="Motherboard" deviceset="DIODE" device="0805" value="TS4148 RYG"/>
<part name="U$24" library="Motherboard" deviceset="DIODE" device="0805" value="TS4148 RYG"/>
<part name="U$25" library="Motherboard" deviceset="DIODE" device="0805" value="TS4148 RYG"/>
<part name="U$26" library="Motherboard" deviceset="DIODE" device="0805" value="TS4148 RYG"/>
<part name="U$27" library="Motherboard" deviceset="DIODE" device="0805" value="TS4148 RYG"/>
<part name="U$28" library="Motherboard" deviceset="DIODE" device="0805" value="TS4148 RYG"/>
<part name="U$29" library="Motherboard" deviceset="DIODE" device="0805" value="TS4148 RYG"/>
<part name="U$30" library="Motherboard" deviceset="DIODE" device="0805" value="TS4148 RYG"/>
<part name="U$31" library="Motherboard" deviceset="DIODE" device="0805" value="TS4148 RYG"/>
<part name="U$32" library="Motherboard" deviceset="DIODE" device="0805" value="TS4148 RYG"/>
</parts>
<sheets>
<sheet>
<description>CPU</description>
<plain>
</plain>
<instances>
<instance part="U1" gate="G$1" x="0" y="0" smashed="yes">
<attribute name="NAME" x="-15.2699" y="28.5037" size="2.54498125" layer="95"/>
<attribute name="VALUE" x="-15.2772" y="-31.0638" size="2.5462" layer="96"/>
</instance>
<instance part="+3V1" gate="G$1" x="25.4" y="33.02" smashed="yes">
<attribute name="VALUE" x="22.86" y="27.94" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND1" gate="1" x="25.4" y="-33.02" smashed="yes">
<attribute name="VALUE" x="22.86" y="-35.56" size="1.778" layer="96"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="+3V3" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="3V3"/>
<wire x1="20.32" y1="25.4" x2="25.4" y2="25.4" width="0.1524" layer="91"/>
<wire x1="25.4" y1="25.4" x2="25.4" y2="30.48" width="0.1524" layer="91"/>
<pinref part="+3V1" gate="G$1" pin="+3V3"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="GND"/>
<wire x1="20.32" y1="-25.4" x2="25.4" y2="-25.4" width="0.1524" layer="91"/>
<wire x1="25.4" y1="-25.4" x2="25.4" y2="-30.48" width="0.1524" layer="91"/>
<pinref part="GND1" gate="1" pin="GND"/>
</segment>
</net>
<net name="MX_COLUMN1" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="IO34"/>
<wire x1="20.32" y1="-17.78" x2="25.4" y2="-17.78" width="0.1524" layer="91"/>
<label x="25.4" y="-17.78" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="MX_COLUMN2" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="IO35"/>
<wire x1="20.32" y1="-20.32" x2="25.4" y2="-20.32" width="0.1524" layer="91"/>
<label x="25.4" y="-20.32" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="MX_COLUMN3" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="SENSOR_VP"/>
<wire x1="-20.32" y1="17.78" x2="-26.67" y2="17.78" width="0.1524" layer="91"/>
<label x="-26.67" y="17.78" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="MX_COLUMN4" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="SENSOR_VN"/>
<wire x1="-20.32" y1="15.24" x2="-26.67" y2="15.24" width="0.1524" layer="91"/>
<label x="-26.67" y="15.24" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="MX_ROW1" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="IO13"/>
<wire x1="20.32" y1="20.32" x2="25.4" y2="20.32" width="0.1524" layer="91"/>
<label x="25.4" y="20.32" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="MX_ROW2" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="IO14"/>
<wire x1="20.32" y1="17.78" x2="25.4" y2="17.78" width="0.1524" layer="91"/>
<label x="25.4" y="17.78" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="MX_ROW3" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="IO15"/>
<wire x1="20.32" y1="15.24" x2="25.4" y2="15.24" width="0.1524" layer="91"/>
<label x="25.4" y="15.24" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="MX_ROW4" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="IO16"/>
<wire x1="20.32" y1="12.7" x2="25.4" y2="12.7" width="0.1524" layer="91"/>
<label x="25.4" y="12.7" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<description>Switches</description>
<plain>
</plain>
<instances>
<instance part="U$1" gate="G$1" x="6.35" y="-35.56" smashed="yes" rot="R90">
<attribute name="NAME" x="13.97" y="-40.64" size="1.778" layer="95" rot="R90"/>
</instance>
<instance part="U$1" gate="G$2" x="2.54" y="49.53" smashed="yes"/>
<instance part="U$2" gate="G$1" x="29.21" y="-35.56" smashed="yes" rot="R90">
<attribute name="NAME" x="36.83" y="-40.64" size="1.778" layer="95" rot="R90"/>
</instance>
<instance part="U$2" gate="G$2" x="22.86" y="49.53" smashed="yes"/>
<instance part="U$3" gate="G$1" x="52.07" y="-35.56" smashed="yes" rot="R90">
<attribute name="NAME" x="59.69" y="-40.64" size="1.778" layer="95" rot="R90"/>
</instance>
<instance part="U$3" gate="G$2" x="43.18" y="49.53" smashed="yes"/>
<instance part="U$4" gate="G$1" x="74.93" y="-35.56" smashed="yes" rot="R90">
<attribute name="NAME" x="82.55" y="-40.64" size="1.778" layer="95" rot="R90"/>
</instance>
<instance part="U$4" gate="G$2" x="63.5" y="49.53" smashed="yes"/>
<instance part="U$5" gate="G$1" x="6.35" y="-60.96" smashed="yes" rot="R90">
<attribute name="NAME" x="13.97" y="-66.04" size="1.778" layer="95" rot="R90"/>
</instance>
<instance part="U$5" gate="G$2" x="2.54" y="27.94" smashed="yes"/>
<instance part="U$6" gate="G$1" x="29.21" y="-60.96" smashed="yes" rot="R90">
<attribute name="NAME" x="36.83" y="-66.04" size="1.778" layer="95" rot="R90"/>
</instance>
<instance part="U$6" gate="G$2" x="22.86" y="27.94" smashed="yes"/>
<instance part="U$7" gate="G$1" x="52.07" y="-60.96" smashed="yes" rot="R90">
<attribute name="NAME" x="59.69" y="-66.04" size="1.778" layer="95" rot="R90"/>
</instance>
<instance part="U$7" gate="G$2" x="43.18" y="27.94" smashed="yes"/>
<instance part="U$8" gate="G$1" x="74.93" y="-60.96" smashed="yes" rot="R90">
<attribute name="NAME" x="82.55" y="-66.04" size="1.778" layer="95" rot="R90"/>
</instance>
<instance part="U$8" gate="G$2" x="63.5" y="27.94" smashed="yes"/>
<instance part="U$9" gate="G$1" x="6.35" y="-86.36" smashed="yes" rot="R90">
<attribute name="NAME" x="13.97" y="-91.44" size="1.778" layer="95" rot="R90"/>
</instance>
<instance part="U$9" gate="G$2" x="2.54" y="6.35" smashed="yes"/>
<instance part="U$10" gate="G$1" x="29.21" y="-86.36" smashed="yes" rot="R90">
<attribute name="NAME" x="36.83" y="-91.44" size="1.778" layer="95" rot="R90"/>
</instance>
<instance part="U$10" gate="G$2" x="22.86" y="6.35" smashed="yes"/>
<instance part="U$11" gate="G$1" x="52.07" y="-86.36" smashed="yes" rot="R90">
<attribute name="NAME" x="59.69" y="-91.44" size="1.778" layer="95" rot="R90"/>
</instance>
<instance part="U$11" gate="G$2" x="43.18" y="6.35" smashed="yes"/>
<instance part="U$12" gate="G$1" x="74.93" y="-86.36" smashed="yes" rot="R90">
<attribute name="NAME" x="82.55" y="-91.44" size="1.778" layer="95" rot="R90"/>
</instance>
<instance part="U$12" gate="G$2" x="63.5" y="6.35" smashed="yes"/>
<instance part="U$13" gate="G$1" x="6.35" y="-111.76" smashed="yes" rot="R90">
<attribute name="NAME" x="13.97" y="-116.84" size="1.778" layer="95" rot="R90"/>
</instance>
<instance part="U$13" gate="G$2" x="2.54" y="-15.24" smashed="yes"/>
<instance part="U$14" gate="G$1" x="29.21" y="-111.76" smashed="yes" rot="R90">
<attribute name="NAME" x="36.83" y="-116.84" size="1.778" layer="95" rot="R90"/>
</instance>
<instance part="U$14" gate="G$2" x="22.86" y="-15.24" smashed="yes"/>
<instance part="U$15" gate="G$1" x="52.07" y="-111.76" smashed="yes" rot="R90">
<attribute name="NAME" x="59.69" y="-116.84" size="1.778" layer="95" rot="R90"/>
</instance>
<instance part="U$15" gate="G$2" x="43.18" y="-15.24" smashed="yes"/>
<instance part="U$16" gate="G$1" x="74.93" y="-111.76" smashed="yes" rot="R90">
<attribute name="NAME" x="82.55" y="-116.84" size="1.778" layer="95" rot="R90"/>
</instance>
<instance part="U$16" gate="G$2" x="63.5" y="-15.24" smashed="yes"/>
<instance part="+3V2" gate="G$1" x="59.69" y="62.23" smashed="yes">
<attribute name="VALUE" x="57.15" y="57.15" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="+3V3" gate="G$1" x="59.69" y="19.05" smashed="yes">
<attribute name="VALUE" x="57.15" y="13.97" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="+3V4" gate="G$1" x="59.69" y="40.64" smashed="yes">
<attribute name="VALUE" x="57.15" y="35.56" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="+3V5" gate="G$1" x="59.69" y="-2.54" smashed="yes">
<attribute name="VALUE" x="57.15" y="-7.62" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND2" gate="1" x="83.82" y="45.72" smashed="yes">
<attribute name="VALUE" x="81.28" y="43.18" size="1.778" layer="96"/>
</instance>
<instance part="GND3" gate="1" x="83.82" y="24.13" smashed="yes">
<attribute name="VALUE" x="81.28" y="21.59" size="1.778" layer="96"/>
</instance>
<instance part="GND4" gate="1" x="83.82" y="2.54" smashed="yes">
<attribute name="VALUE" x="81.28" y="0" size="1.778" layer="96"/>
</instance>
<instance part="GND5" gate="1" x="83.82" y="-19.05" smashed="yes">
<attribute name="VALUE" x="81.28" y="-21.59" size="1.778" layer="96"/>
</instance>
<instance part="U$17" gate="G$1" x="-1.27" y="-49.53" smashed="yes"/>
<instance part="U$18" gate="G$1" x="21.59" y="-49.53" smashed="yes"/>
<instance part="U$19" gate="G$1" x="44.45" y="-49.53" smashed="yes"/>
<instance part="U$20" gate="G$1" x="67.31" y="-49.53" smashed="yes"/>
<instance part="U$21" gate="G$1" x="-1.27" y="-74.93" smashed="yes"/>
<instance part="U$22" gate="G$1" x="21.59" y="-74.93" smashed="yes"/>
<instance part="U$23" gate="G$1" x="44.45" y="-74.93" smashed="yes"/>
<instance part="U$24" gate="G$1" x="67.31" y="-74.93" smashed="yes"/>
<instance part="U$25" gate="G$1" x="-1.27" y="-100.33" smashed="yes"/>
<instance part="U$26" gate="G$1" x="21.59" y="-100.33" smashed="yes"/>
<instance part="U$27" gate="G$1" x="44.45" y="-100.33" smashed="yes"/>
<instance part="U$28" gate="G$1" x="67.31" y="-100.33" smashed="yes"/>
<instance part="U$29" gate="G$1" x="-1.27" y="-125.73" smashed="yes"/>
<instance part="U$30" gate="G$1" x="21.59" y="-125.73" smashed="yes"/>
<instance part="U$31" gate="G$1" x="44.45" y="-125.73" smashed="yes"/>
<instance part="U$32" gate="G$1" x="67.31" y="-125.73" smashed="yes"/>
</instances>
<busses>
</busses>
<nets>
<net name="N$1" class="0">
<segment>
<pinref part="U$1" gate="G$2" pin="DOUT"/>
<pinref part="U$2" gate="G$2" pin="DIN"/>
<wire x1="9.525" y1="44.45" x2="20.32" y2="44.45" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="U$2" gate="G$2" pin="DOUT"/>
<pinref part="U$3" gate="G$2" pin="DIN"/>
<wire x1="29.845" y1="44.45" x2="40.64" y2="44.45" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="U$3" gate="G$2" pin="DOUT"/>
<pinref part="U$4" gate="G$2" pin="DIN"/>
<wire x1="50.165" y1="44.45" x2="60.96" y2="44.45" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="U$5" gate="G$2" pin="DOUT"/>
<pinref part="U$6" gate="G$2" pin="DIN"/>
<wire x1="9.525" y1="22.86" x2="20.32" y2="22.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="U$6" gate="G$2" pin="DOUT"/>
<pinref part="U$7" gate="G$2" pin="DIN"/>
<wire x1="29.845" y1="22.86" x2="40.64" y2="22.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="U$7" gate="G$2" pin="DOUT"/>
<pinref part="U$8" gate="G$2" pin="DIN"/>
<wire x1="50.165" y1="22.86" x2="60.96" y2="22.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="U$9" gate="G$2" pin="DOUT"/>
<pinref part="U$10" gate="G$2" pin="DIN"/>
<wire x1="9.525" y1="1.27" x2="20.32" y2="1.27" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="U$10" gate="G$2" pin="DOUT"/>
<pinref part="U$11" gate="G$2" pin="DIN"/>
<wire x1="29.845" y1="1.27" x2="40.64" y2="1.27" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="U$11" gate="G$2" pin="DOUT"/>
<pinref part="U$12" gate="G$2" pin="DIN"/>
<wire x1="50.165" y1="1.27" x2="60.96" y2="1.27" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="U$13" gate="G$2" pin="DOUT"/>
<pinref part="U$14" gate="G$2" pin="DIN"/>
<wire x1="9.525" y1="-20.32" x2="20.32" y2="-20.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="U$14" gate="G$2" pin="DOUT"/>
<pinref part="U$15" gate="G$2" pin="DIN"/>
<wire x1="29.845" y1="-20.32" x2="40.64" y2="-20.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="U$15" gate="G$2" pin="DOUT"/>
<pinref part="U$16" gate="G$2" pin="DIN"/>
<wire x1="50.165" y1="-20.32" x2="60.96" y2="-20.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DIN" class="0">
<segment>
<pinref part="U$1" gate="G$2" pin="DIN"/>
<wire x1="0" y1="44.45" x2="-6.35" y2="44.45" width="0.1524" layer="91"/>
<label x="-6.35" y="44.45" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="D1" class="0">
<segment>
<pinref part="U$4" gate="G$2" pin="DOUT"/>
<wire x1="70.485" y1="44.45" x2="73.66" y2="44.45" width="0.1524" layer="91"/>
<label x="73.66" y="44.45" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U$5" gate="G$2" pin="DIN"/>
<wire x1="-6.35" y1="22.86" x2="0" y2="22.86" width="0.1524" layer="91"/>
<label x="-6.35" y="22.86" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="D2" class="0">
<segment>
<pinref part="U$8" gate="G$2" pin="DOUT"/>
<wire x1="70.485" y1="22.86" x2="73.66" y2="22.86" width="0.1524" layer="91"/>
<label x="73.66" y="22.86" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U$9" gate="G$2" pin="DIN"/>
<wire x1="-6.35" y1="1.27" x2="0" y2="1.27" width="0.1524" layer="91"/>
<label x="-6.35" y="1.27" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="D3" class="0">
<segment>
<pinref part="U$12" gate="G$2" pin="DOUT"/>
<wire x1="70.485" y1="1.27" x2="73.66" y2="1.27" width="0.1524" layer="91"/>
<label x="73.66" y="1.27" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U$13" gate="G$2" pin="DIN"/>
<wire x1="-6.35" y1="-20.32" x2="0" y2="-20.32" width="0.1524" layer="91"/>
<label x="-6.35" y="-20.32" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="U$13" gate="G$2" pin="VDD"/>
<wire x1="0" y1="-15.24" x2="-3.81" y2="-15.24" width="0.1524" layer="91"/>
<wire x1="-3.81" y1="-15.24" x2="-3.81" y2="-8.89" width="0.1524" layer="91"/>
<wire x1="-3.81" y1="-8.89" x2="19.05" y2="-8.89" width="0.1524" layer="91"/>
<pinref part="U$14" gate="G$2" pin="VDD"/>
<wire x1="19.05" y1="-8.89" x2="19.05" y2="-15.24" width="0.1524" layer="91"/>
<wire x1="19.05" y1="-15.24" x2="20.32" y2="-15.24" width="0.1524" layer="91"/>
<wire x1="19.05" y1="-8.89" x2="39.37" y2="-8.89" width="0.1524" layer="91"/>
<wire x1="39.37" y1="-8.89" x2="39.37" y2="-15.24" width="0.1524" layer="91"/>
<junction x="19.05" y="-8.89"/>
<pinref part="U$15" gate="G$2" pin="VDD"/>
<wire x1="39.37" y1="-15.24" x2="40.64" y2="-15.24" width="0.1524" layer="91"/>
<wire x1="39.37" y1="-8.89" x2="59.69" y2="-8.89" width="0.1524" layer="91"/>
<wire x1="59.69" y1="-8.89" x2="59.69" y2="-15.24" width="0.1524" layer="91"/>
<junction x="39.37" y="-8.89"/>
<pinref part="U$16" gate="G$2" pin="VDD"/>
<wire x1="59.69" y1="-15.24" x2="60.96" y2="-15.24" width="0.1524" layer="91"/>
<pinref part="+3V5" gate="G$1" pin="+3V3"/>
<wire x1="59.69" y1="-8.89" x2="59.69" y2="-5.08" width="0.1524" layer="91"/>
<junction x="59.69" y="-8.89"/>
</segment>
<segment>
<pinref part="U$2" gate="G$2" pin="VDD"/>
<wire x1="20.32" y1="49.53" x2="20.32" y2="55.88" width="0.1524" layer="91"/>
<wire x1="20.32" y1="55.88" x2="-3.81" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$2" pin="VDD"/>
<wire x1="-3.81" y1="55.88" x2="-3.81" y2="49.53" width="0.1524" layer="91"/>
<wire x1="-3.81" y1="49.53" x2="0" y2="49.53" width="0.1524" layer="91"/>
<wire x1="20.32" y1="55.88" x2="39.37" y2="55.88" width="0.1524" layer="91"/>
<wire x1="39.37" y1="55.88" x2="39.37" y2="49.53" width="0.1524" layer="91"/>
<junction x="20.32" y="55.88"/>
<pinref part="U$3" gate="G$2" pin="VDD"/>
<wire x1="39.37" y1="49.53" x2="40.64" y2="49.53" width="0.1524" layer="91"/>
<pinref part="U$4" gate="G$2" pin="VDD"/>
<wire x1="60.96" y1="49.53" x2="59.69" y2="49.53" width="0.1524" layer="91"/>
<wire x1="59.69" y1="49.53" x2="59.69" y2="55.88" width="0.1524" layer="91"/>
<wire x1="59.69" y1="55.88" x2="39.37" y2="55.88" width="0.1524" layer="91"/>
<junction x="39.37" y="55.88"/>
<pinref part="+3V2" gate="G$1" pin="+3V3"/>
<wire x1="59.69" y1="55.88" x2="59.69" y2="59.69" width="0.1524" layer="91"/>
<junction x="59.69" y="55.88"/>
</segment>
<segment>
<pinref part="U$5" gate="G$2" pin="VDD"/>
<wire x1="0" y1="27.94" x2="-3.81" y2="27.94" width="0.1524" layer="91"/>
<wire x1="-3.81" y1="27.94" x2="-3.81" y2="34.29" width="0.1524" layer="91"/>
<wire x1="-3.81" y1="34.29" x2="19.05" y2="34.29" width="0.1524" layer="91"/>
<pinref part="U$6" gate="G$2" pin="VDD"/>
<wire x1="19.05" y1="34.29" x2="19.05" y2="27.94" width="0.1524" layer="91"/>
<wire x1="19.05" y1="27.94" x2="20.32" y2="27.94" width="0.1524" layer="91"/>
<wire x1="19.05" y1="34.29" x2="39.37" y2="34.29" width="0.1524" layer="91"/>
<wire x1="39.37" y1="34.29" x2="39.37" y2="27.94" width="0.1524" layer="91"/>
<junction x="19.05" y="34.29"/>
<pinref part="U$7" gate="G$2" pin="VDD"/>
<wire x1="39.37" y1="27.94" x2="40.64" y2="27.94" width="0.1524" layer="91"/>
<wire x1="39.37" y1="34.29" x2="59.69" y2="34.29" width="0.1524" layer="91"/>
<wire x1="59.69" y1="34.29" x2="59.69" y2="27.94" width="0.1524" layer="91"/>
<junction x="39.37" y="34.29"/>
<pinref part="U$8" gate="G$2" pin="VDD"/>
<wire x1="59.69" y1="27.94" x2="60.96" y2="27.94" width="0.1524" layer="91"/>
<pinref part="+3V4" gate="G$1" pin="+3V3"/>
<wire x1="59.69" y1="38.1" x2="59.69" y2="34.29" width="0.1524" layer="91"/>
<junction x="59.69" y="34.29"/>
</segment>
<segment>
<pinref part="U$9" gate="G$2" pin="VDD"/>
<wire x1="0" y1="6.35" x2="-3.81" y2="6.35" width="0.1524" layer="91"/>
<wire x1="-3.81" y1="6.35" x2="-3.81" y2="12.7" width="0.1524" layer="91"/>
<wire x1="-3.81" y1="12.7" x2="19.05" y2="12.7" width="0.1524" layer="91"/>
<wire x1="19.05" y1="12.7" x2="19.05" y2="6.35" width="0.1524" layer="91"/>
<pinref part="U$10" gate="G$2" pin="VDD"/>
<wire x1="19.05" y1="6.35" x2="20.32" y2="6.35" width="0.1524" layer="91"/>
<wire x1="19.05" y1="12.7" x2="39.37" y2="12.7" width="0.1524" layer="91"/>
<wire x1="39.37" y1="12.7" x2="39.37" y2="6.35" width="0.1524" layer="91"/>
<junction x="19.05" y="12.7"/>
<pinref part="U$11" gate="G$2" pin="VDD"/>
<wire x1="39.37" y1="6.35" x2="40.64" y2="6.35" width="0.1524" layer="91"/>
<wire x1="39.37" y1="12.7" x2="59.69" y2="12.7" width="0.1524" layer="91"/>
<wire x1="59.69" y1="12.7" x2="59.69" y2="6.35" width="0.1524" layer="91"/>
<junction x="39.37" y="12.7"/>
<pinref part="U$12" gate="G$2" pin="VDD"/>
<wire x1="59.69" y1="6.35" x2="60.96" y2="6.35" width="0.1524" layer="91"/>
<pinref part="+3V3" gate="G$1" pin="+3V3"/>
<wire x1="59.69" y1="16.51" x2="59.69" y2="12.7" width="0.1524" layer="91"/>
<junction x="59.69" y="12.7"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="U$13" gate="G$2" pin="VSS"/>
<wire x1="9.525" y1="-15.24" x2="10.16" y2="-15.24" width="0.1524" layer="91"/>
<wire x1="10.16" y1="-15.24" x2="10.16" y2="-11.43" width="0.1524" layer="91"/>
<wire x1="10.16" y1="-11.43" x2="30.48" y2="-11.43" width="0.1524" layer="91"/>
<wire x1="30.48" y1="-11.43" x2="30.48" y2="-15.24" width="0.1524" layer="91"/>
<pinref part="U$14" gate="G$2" pin="VSS"/>
<wire x1="30.48" y1="-15.24" x2="29.845" y2="-15.24" width="0.1524" layer="91"/>
<pinref part="U$15" gate="G$2" pin="VSS"/>
<wire x1="50.8" y1="-15.24" x2="50.165" y2="-15.24" width="0.1524" layer="91"/>
<wire x1="30.48" y1="-11.43" x2="50.8" y2="-11.43" width="0.1524" layer="91"/>
<wire x1="50.8" y1="-11.43" x2="50.8" y2="-15.24" width="0.1524" layer="91"/>
<junction x="30.48" y="-11.43"/>
<wire x1="50.8" y1="-11.43" x2="71.12" y2="-11.43" width="0.1524" layer="91"/>
<wire x1="71.12" y1="-11.43" x2="71.12" y2="-15.24" width="0.1524" layer="91"/>
<junction x="50.8" y="-11.43"/>
<pinref part="U$16" gate="G$2" pin="VSS"/>
<wire x1="71.12" y1="-15.24" x2="70.485" y2="-15.24" width="0.1524" layer="91"/>
<pinref part="GND5" gate="1" pin="GND"/>
<wire x1="71.12" y1="-11.43" x2="83.82" y2="-11.43" width="0.1524" layer="91"/>
<wire x1="83.82" y1="-11.43" x2="83.82" y2="-16.51" width="0.1524" layer="91"/>
<junction x="71.12" y="-11.43"/>
</segment>
<segment>
<pinref part="U$12" gate="G$2" pin="VSS"/>
<wire x1="70.485" y1="6.35" x2="71.12" y2="6.35" width="0.1524" layer="91"/>
<wire x1="71.12" y1="6.35" x2="71.12" y2="10.16" width="0.1524" layer="91"/>
<wire x1="71.12" y1="10.16" x2="50.8" y2="10.16" width="0.1524" layer="91"/>
<wire x1="50.8" y1="10.16" x2="50.8" y2="6.35" width="0.1524" layer="91"/>
<pinref part="U$11" gate="G$2" pin="VSS"/>
<wire x1="50.8" y1="6.35" x2="50.165" y2="6.35" width="0.1524" layer="91"/>
<wire x1="50.8" y1="10.16" x2="30.48" y2="10.16" width="0.1524" layer="91"/>
<wire x1="30.48" y1="10.16" x2="30.48" y2="6.35" width="0.1524" layer="91"/>
<junction x="50.8" y="10.16"/>
<pinref part="U$10" gate="G$2" pin="VSS"/>
<wire x1="30.48" y1="6.35" x2="29.845" y2="6.35" width="0.1524" layer="91"/>
<wire x1="30.48" y1="10.16" x2="10.16" y2="10.16" width="0.1524" layer="91"/>
<wire x1="10.16" y1="10.16" x2="10.16" y2="6.35" width="0.1524" layer="91"/>
<junction x="30.48" y="10.16"/>
<pinref part="U$9" gate="G$2" pin="VSS"/>
<wire x1="10.16" y1="6.35" x2="9.525" y2="6.35" width="0.1524" layer="91"/>
<pinref part="GND4" gate="1" pin="GND"/>
<wire x1="71.12" y1="10.16" x2="83.82" y2="10.16" width="0.1524" layer="91"/>
<wire x1="83.82" y1="10.16" x2="83.82" y2="5.08" width="0.1524" layer="91"/>
<junction x="71.12" y="10.16"/>
</segment>
<segment>
<pinref part="U$5" gate="G$2" pin="VSS"/>
<wire x1="10.16" y1="31.75" x2="10.16" y2="27.94" width="0.1524" layer="91"/>
<wire x1="10.16" y1="27.94" x2="9.525" y2="27.94" width="0.1524" layer="91"/>
<wire x1="10.16" y1="31.75" x2="30.48" y2="31.75" width="0.1524" layer="91"/>
<wire x1="30.48" y1="31.75" x2="30.48" y2="27.94" width="0.1524" layer="91"/>
<pinref part="U$6" gate="G$2" pin="VSS"/>
<wire x1="30.48" y1="27.94" x2="29.845" y2="27.94" width="0.1524" layer="91"/>
<wire x1="30.48" y1="31.75" x2="50.8" y2="31.75" width="0.1524" layer="91"/>
<wire x1="50.8" y1="31.75" x2="50.8" y2="27.94" width="0.1524" layer="91"/>
<junction x="30.48" y="31.75"/>
<pinref part="U$7" gate="G$2" pin="VSS"/>
<wire x1="50.8" y1="27.94" x2="50.165" y2="27.94" width="0.1524" layer="91"/>
<wire x1="50.8" y1="31.75" x2="71.12" y2="31.75" width="0.1524" layer="91"/>
<wire x1="71.12" y1="31.75" x2="71.12" y2="27.94" width="0.1524" layer="91"/>
<junction x="50.8" y="31.75"/>
<pinref part="U$8" gate="G$2" pin="VSS"/>
<wire x1="71.12" y1="27.94" x2="70.485" y2="27.94" width="0.1524" layer="91"/>
<pinref part="GND3" gate="1" pin="GND"/>
<wire x1="71.12" y1="31.75" x2="83.82" y2="31.75" width="0.1524" layer="91"/>
<wire x1="83.82" y1="31.75" x2="83.82" y2="26.67" width="0.1524" layer="91"/>
<junction x="71.12" y="31.75"/>
</segment>
<segment>
<pinref part="U$1" gate="G$2" pin="VSS"/>
<wire x1="9.525" y1="49.53" x2="10.16" y2="49.53" width="0.1524" layer="91"/>
<wire x1="10.16" y1="49.53" x2="10.16" y2="53.34" width="0.1524" layer="91"/>
<wire x1="10.16" y1="53.34" x2="30.48" y2="53.34" width="0.1524" layer="91"/>
<wire x1="30.48" y1="53.34" x2="30.48" y2="49.53" width="0.1524" layer="91"/>
<pinref part="U$2" gate="G$2" pin="VSS"/>
<wire x1="30.48" y1="49.53" x2="29.845" y2="49.53" width="0.1524" layer="91"/>
<wire x1="30.48" y1="53.34" x2="50.8" y2="53.34" width="0.1524" layer="91"/>
<wire x1="50.8" y1="53.34" x2="50.8" y2="49.53" width="0.1524" layer="91"/>
<junction x="30.48" y="53.34"/>
<wire x1="50.8" y1="49.53" x2="50.165" y2="49.53" width="0.1524" layer="91"/>
<pinref part="U$3" gate="G$2" pin="VSS"/>
<wire x1="50.8" y1="53.34" x2="71.12" y2="53.34" width="0.1524" layer="91"/>
<wire x1="71.12" y1="53.34" x2="71.12" y2="49.53" width="0.1524" layer="91"/>
<junction x="50.8" y="53.34"/>
<pinref part="U$4" gate="G$2" pin="VSS"/>
<wire x1="71.12" y1="49.53" x2="70.485" y2="49.53" width="0.1524" layer="91"/>
<pinref part="GND2" gate="1" pin="GND"/>
<wire x1="71.12" y1="53.34" x2="83.82" y2="53.34" width="0.1524" layer="91"/>
<wire x1="83.82" y1="53.34" x2="83.82" y2="48.26" width="0.1524" layer="91"/>
<junction x="71.12" y="53.34"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="U$17" gate="G$1" pin="P$2"/>
<pinref part="U$1" gate="G$1" pin="PIN-1"/>
<wire x1="2.54" y1="-49.53" x2="3.81" y2="-49.53" width="0.1524" layer="91"/>
<wire x1="3.81" y1="-49.53" x2="3.81" y2="-45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="U$18" gate="G$1" pin="P$2"/>
<pinref part="U$2" gate="G$1" pin="PIN-1"/>
<wire x1="25.4" y1="-49.53" x2="26.67" y2="-49.53" width="0.1524" layer="91"/>
<wire x1="26.67" y1="-49.53" x2="26.67" y2="-45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="U$19" gate="G$1" pin="P$2"/>
<pinref part="U$3" gate="G$1" pin="PIN-1"/>
<wire x1="48.26" y1="-49.53" x2="49.53" y2="-49.53" width="0.1524" layer="91"/>
<wire x1="49.53" y1="-49.53" x2="49.53" y2="-45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="U$20" gate="G$1" pin="P$2"/>
<pinref part="U$4" gate="G$1" pin="PIN-1"/>
<wire x1="71.12" y1="-49.53" x2="72.39" y2="-49.53" width="0.1524" layer="91"/>
<wire x1="72.39" y1="-49.53" x2="72.39" y2="-45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="U$24" gate="G$1" pin="P$2"/>
<pinref part="U$8" gate="G$1" pin="PIN-1"/>
<wire x1="71.12" y1="-74.93" x2="72.39" y2="-74.93" width="0.1524" layer="91"/>
<wire x1="72.39" y1="-74.93" x2="72.39" y2="-71.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="U$23" gate="G$1" pin="P$2"/>
<pinref part="U$7" gate="G$1" pin="PIN-1"/>
<wire x1="48.26" y1="-74.93" x2="49.53" y2="-74.93" width="0.1524" layer="91"/>
<wire x1="49.53" y1="-74.93" x2="49.53" y2="-71.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="U$22" gate="G$1" pin="P$2"/>
<wire x1="25.4" y1="-74.93" x2="26.67" y2="-74.93" width="0.1524" layer="91"/>
<pinref part="U$6" gate="G$1" pin="PIN-1"/>
<wire x1="26.67" y1="-74.93" x2="26.67" y2="-71.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="U$21" gate="G$1" pin="P$2"/>
<pinref part="U$5" gate="G$1" pin="PIN-1"/>
<wire x1="2.54" y1="-74.93" x2="3.81" y2="-74.93" width="0.1524" layer="91"/>
<wire x1="3.81" y1="-74.93" x2="3.81" y2="-71.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="U$25" gate="G$1" pin="P$2"/>
<pinref part="U$9" gate="G$1" pin="PIN-1"/>
<wire x1="2.54" y1="-100.33" x2="3.81" y2="-100.33" width="0.1524" layer="91"/>
<wire x1="3.81" y1="-100.33" x2="3.81" y2="-96.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="U$26" gate="G$1" pin="P$2"/>
<pinref part="U$10" gate="G$1" pin="PIN-1"/>
<wire x1="25.4" y1="-100.33" x2="26.67" y2="-100.33" width="0.1524" layer="91"/>
<wire x1="26.67" y1="-100.33" x2="26.67" y2="-96.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="U$27" gate="G$1" pin="P$2"/>
<pinref part="U$11" gate="G$1" pin="PIN-1"/>
<wire x1="48.26" y1="-100.33" x2="49.53" y2="-100.33" width="0.1524" layer="91"/>
<wire x1="49.53" y1="-100.33" x2="49.53" y2="-96.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<pinref part="U$28" gate="G$1" pin="P$2"/>
<pinref part="U$12" gate="G$1" pin="PIN-1"/>
<wire x1="71.12" y1="-100.33" x2="72.39" y2="-100.33" width="0.1524" layer="91"/>
<wire x1="72.39" y1="-100.33" x2="72.39" y2="-96.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<pinref part="U$32" gate="G$1" pin="P$2"/>
<pinref part="U$16" gate="G$1" pin="PIN-1"/>
<wire x1="71.12" y1="-125.73" x2="72.39" y2="-125.73" width="0.1524" layer="91"/>
<wire x1="72.39" y1="-125.73" x2="72.39" y2="-121.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<pinref part="U$29" gate="G$1" pin="P$2"/>
<pinref part="U$13" gate="G$1" pin="PIN-1"/>
<wire x1="2.54" y1="-125.73" x2="3.81" y2="-125.73" width="0.1524" layer="91"/>
<wire x1="3.81" y1="-125.73" x2="3.81" y2="-121.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<pinref part="U$30" gate="G$1" pin="P$2"/>
<pinref part="U$14" gate="G$1" pin="PIN-1"/>
<wire x1="25.4" y1="-125.73" x2="26.67" y2="-125.73" width="0.1524" layer="91"/>
<wire x1="26.67" y1="-125.73" x2="26.67" y2="-121.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$28" class="0">
<segment>
<pinref part="U$31" gate="G$1" pin="P$2"/>
<pinref part="U$15" gate="G$1" pin="PIN-1"/>
<wire x1="48.26" y1="-125.73" x2="49.53" y2="-125.73" width="0.1524" layer="91"/>
<wire x1="49.53" y1="-125.73" x2="49.53" y2="-121.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="MX_ROW4" class="0">
<segment>
<pinref part="U$32" gate="G$1" pin="P$1"/>
<wire x1="66.04" y1="-125.73" x2="64.77" y2="-125.73" width="0.1524" layer="91"/>
<wire x1="64.77" y1="-125.73" x2="64.77" y2="-129.54" width="0.1524" layer="91"/>
<wire x1="64.77" y1="-129.54" x2="41.91" y2="-129.54" width="0.1524" layer="91"/>
<pinref part="U$29" gate="G$1" pin="P$1"/>
<wire x1="41.91" y1="-129.54" x2="19.05" y2="-129.54" width="0.1524" layer="91"/>
<wire x1="19.05" y1="-129.54" x2="-3.81" y2="-129.54" width="0.1524" layer="91"/>
<wire x1="-3.81" y1="-129.54" x2="-10.16" y2="-129.54" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="-125.73" x2="-3.81" y2="-125.73" width="0.1524" layer="91"/>
<wire x1="-3.81" y1="-125.73" x2="-3.81" y2="-129.54" width="0.1524" layer="91"/>
<junction x="-3.81" y="-129.54"/>
<pinref part="U$30" gate="G$1" pin="P$1"/>
<wire x1="20.32" y1="-125.73" x2="19.05" y2="-125.73" width="0.1524" layer="91"/>
<wire x1="19.05" y1="-125.73" x2="19.05" y2="-129.54" width="0.1524" layer="91"/>
<junction x="19.05" y="-129.54"/>
<pinref part="U$31" gate="G$1" pin="P$1"/>
<wire x1="43.18" y1="-125.73" x2="41.91" y2="-125.73" width="0.1524" layer="91"/>
<wire x1="41.91" y1="-125.73" x2="41.91" y2="-129.54" width="0.1524" layer="91"/>
<junction x="41.91" y="-129.54"/>
<label x="-10.16" y="-129.54" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="MX_ROW3" class="0">
<segment>
<pinref part="U$28" gate="G$1" pin="P$1"/>
<wire x1="66.04" y1="-100.33" x2="64.77" y2="-100.33" width="0.1524" layer="91"/>
<wire x1="64.77" y1="-100.33" x2="64.77" y2="-104.14" width="0.1524" layer="91"/>
<wire x1="64.77" y1="-104.14" x2="41.91" y2="-104.14" width="0.1524" layer="91"/>
<pinref part="U$25" gate="G$1" pin="P$1"/>
<wire x1="41.91" y1="-104.14" x2="19.05" y2="-104.14" width="0.1524" layer="91"/>
<wire x1="19.05" y1="-104.14" x2="-3.81" y2="-104.14" width="0.1524" layer="91"/>
<wire x1="-3.81" y1="-104.14" x2="-10.16" y2="-104.14" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="-100.33" x2="-3.81" y2="-100.33" width="0.1524" layer="91"/>
<wire x1="-3.81" y1="-100.33" x2="-3.81" y2="-104.14" width="0.1524" layer="91"/>
<junction x="-3.81" y="-104.14"/>
<pinref part="U$26" gate="G$1" pin="P$1"/>
<wire x1="20.32" y1="-100.33" x2="19.05" y2="-100.33" width="0.1524" layer="91"/>
<wire x1="19.05" y1="-100.33" x2="19.05" y2="-104.14" width="0.1524" layer="91"/>
<junction x="19.05" y="-104.14"/>
<pinref part="U$27" gate="G$1" pin="P$1"/>
<wire x1="43.18" y1="-100.33" x2="41.91" y2="-100.33" width="0.1524" layer="91"/>
<wire x1="41.91" y1="-100.33" x2="41.91" y2="-104.14" width="0.1524" layer="91"/>
<junction x="41.91" y="-104.14"/>
<label x="-10.16" y="-104.14" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="MX_COLUMN3" class="0">
<segment>
<pinref part="U$11" gate="G$1" pin="PIN-2"/>
<wire x1="54.61" y1="-96.52" x2="54.61" y2="-100.33" width="0.1524" layer="91"/>
<wire x1="54.61" y1="-100.33" x2="60.96" y2="-100.33" width="0.1524" layer="91"/>
<wire x1="60.96" y1="-100.33" x2="60.96" y2="-125.73" width="0.1524" layer="91"/>
<pinref part="U$15" gate="G$1" pin="PIN-2"/>
<wire x1="60.96" y1="-125.73" x2="60.96" y2="-134.62" width="0.1524" layer="91"/>
<wire x1="54.61" y1="-121.92" x2="54.61" y2="-125.73" width="0.1524" layer="91"/>
<wire x1="54.61" y1="-125.73" x2="60.96" y2="-125.73" width="0.1524" layer="91"/>
<junction x="60.96" y="-125.73"/>
<wire x1="60.96" y1="-100.33" x2="60.96" y2="-74.93" width="0.1524" layer="91"/>
<wire x1="60.96" y1="-74.93" x2="54.61" y2="-74.93" width="0.1524" layer="91"/>
<junction x="60.96" y="-100.33"/>
<pinref part="U$7" gate="G$1" pin="PIN-2"/>
<wire x1="54.61" y1="-74.93" x2="54.61" y2="-71.12" width="0.1524" layer="91"/>
<wire x1="60.96" y1="-74.93" x2="60.96" y2="-49.53" width="0.1524" layer="91"/>
<wire x1="60.96" y1="-49.53" x2="54.61" y2="-49.53" width="0.1524" layer="91"/>
<junction x="60.96" y="-74.93"/>
<pinref part="U$3" gate="G$1" pin="PIN-2"/>
<wire x1="54.61" y1="-49.53" x2="54.61" y2="-45.72" width="0.1524" layer="91"/>
<label x="60.96" y="-134.62" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
</net>
<net name="MX_ROW1" class="0">
<segment>
<pinref part="U$17" gate="G$1" pin="P$1"/>
<wire x1="-2.54" y1="-49.53" x2="-3.81" y2="-49.53" width="0.1524" layer="91"/>
<wire x1="-3.81" y1="-49.53" x2="-3.81" y2="-53.34" width="0.1524" layer="91"/>
<wire x1="-3.81" y1="-53.34" x2="19.05" y2="-53.34" width="0.1524" layer="91"/>
<wire x1="19.05" y1="-53.34" x2="19.05" y2="-49.53" width="0.1524" layer="91"/>
<pinref part="U$18" gate="G$1" pin="P$1"/>
<wire x1="19.05" y1="-49.53" x2="20.32" y2="-49.53" width="0.1524" layer="91"/>
<wire x1="19.05" y1="-53.34" x2="41.91" y2="-53.34" width="0.1524" layer="91"/>
<wire x1="41.91" y1="-53.34" x2="41.91" y2="-49.53" width="0.1524" layer="91"/>
<junction x="19.05" y="-53.34"/>
<pinref part="U$19" gate="G$1" pin="P$1"/>
<wire x1="41.91" y1="-49.53" x2="43.18" y2="-49.53" width="0.1524" layer="91"/>
<wire x1="41.91" y1="-53.34" x2="64.77" y2="-53.34" width="0.1524" layer="91"/>
<wire x1="64.77" y1="-53.34" x2="64.77" y2="-49.53" width="0.1524" layer="91"/>
<junction x="41.91" y="-53.34"/>
<pinref part="U$20" gate="G$1" pin="P$1"/>
<wire x1="64.77" y1="-49.53" x2="66.04" y2="-49.53" width="0.1524" layer="91"/>
<wire x1="-3.81" y1="-53.34" x2="-10.16" y2="-53.34" width="0.1524" layer="91"/>
<junction x="-3.81" y="-53.34"/>
<label x="-10.16" y="-53.34" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="MX_ROW2" class="0">
<segment>
<pinref part="U$21" gate="G$1" pin="P$1"/>
<wire x1="-2.54" y1="-74.93" x2="-3.81" y2="-74.93" width="0.1524" layer="91"/>
<wire x1="-3.81" y1="-74.93" x2="-3.81" y2="-78.74" width="0.1524" layer="91"/>
<wire x1="-3.81" y1="-78.74" x2="19.05" y2="-78.74" width="0.1524" layer="91"/>
<wire x1="19.05" y1="-78.74" x2="19.05" y2="-74.93" width="0.1524" layer="91"/>
<pinref part="U$22" gate="G$1" pin="P$1"/>
<wire x1="19.05" y1="-74.93" x2="20.32" y2="-74.93" width="0.1524" layer="91"/>
<wire x1="19.05" y1="-78.74" x2="41.91" y2="-78.74" width="0.1524" layer="91"/>
<wire x1="41.91" y1="-78.74" x2="41.91" y2="-74.93" width="0.1524" layer="91"/>
<junction x="19.05" y="-78.74"/>
<pinref part="U$23" gate="G$1" pin="P$1"/>
<wire x1="41.91" y1="-74.93" x2="43.18" y2="-74.93" width="0.1524" layer="91"/>
<wire x1="41.91" y1="-78.74" x2="64.77" y2="-78.74" width="0.1524" layer="91"/>
<wire x1="64.77" y1="-78.74" x2="64.77" y2="-74.93" width="0.1524" layer="91"/>
<junction x="41.91" y="-78.74"/>
<pinref part="U$24" gate="G$1" pin="P$1"/>
<wire x1="64.77" y1="-74.93" x2="66.04" y2="-74.93" width="0.1524" layer="91"/>
<wire x1="-3.81" y1="-78.74" x2="-10.16" y2="-78.74" width="0.1524" layer="91"/>
<junction x="-3.81" y="-78.74"/>
<label x="-10.16" y="-78.74" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="MX_COLUMN4" class="0">
<segment>
<pinref part="U$4" gate="G$1" pin="PIN-2"/>
<wire x1="77.47" y1="-45.72" x2="77.47" y2="-49.53" width="0.1524" layer="91"/>
<wire x1="77.47" y1="-49.53" x2="83.82" y2="-49.53" width="0.1524" layer="91"/>
<wire x1="83.82" y1="-49.53" x2="83.82" y2="-74.93" width="0.1524" layer="91"/>
<wire x1="83.82" y1="-74.93" x2="77.47" y2="-74.93" width="0.1524" layer="91"/>
<pinref part="U$8" gate="G$1" pin="PIN-2"/>
<wire x1="77.47" y1="-74.93" x2="77.47" y2="-71.12" width="0.1524" layer="91"/>
<wire x1="83.82" y1="-74.93" x2="83.82" y2="-100.33" width="0.1524" layer="91"/>
<wire x1="83.82" y1="-100.33" x2="77.47" y2="-100.33" width="0.1524" layer="91"/>
<junction x="83.82" y="-74.93"/>
<pinref part="U$12" gate="G$1" pin="PIN-2"/>
<wire x1="77.47" y1="-100.33" x2="77.47" y2="-96.52" width="0.1524" layer="91"/>
<wire x1="83.82" y1="-100.33" x2="83.82" y2="-125.73" width="0.1524" layer="91"/>
<wire x1="83.82" y1="-125.73" x2="77.47" y2="-125.73" width="0.1524" layer="91"/>
<junction x="83.82" y="-100.33"/>
<pinref part="U$16" gate="G$1" pin="PIN-2"/>
<wire x1="77.47" y1="-125.73" x2="77.47" y2="-121.92" width="0.1524" layer="91"/>
<wire x1="83.82" y1="-125.73" x2="83.82" y2="-134.62" width="0.1524" layer="91"/>
<junction x="83.82" y="-125.73"/>
<label x="83.82" y="-134.62" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
</net>
<net name="MX_COLUMN2" class="0">
<segment>
<pinref part="U$10" gate="G$1" pin="PIN-2"/>
<wire x1="31.75" y1="-96.52" x2="31.75" y2="-100.33" width="0.1524" layer="91"/>
<wire x1="31.75" y1="-100.33" x2="38.1" y2="-100.33" width="0.1524" layer="91"/>
<wire x1="38.1" y1="-100.33" x2="38.1" y2="-125.73" width="0.1524" layer="91"/>
<wire x1="38.1" y1="-125.73" x2="31.75" y2="-125.73" width="0.1524" layer="91"/>
<pinref part="U$14" gate="G$1" pin="PIN-2"/>
<wire x1="31.75" y1="-125.73" x2="31.75" y2="-121.92" width="0.1524" layer="91"/>
<wire x1="38.1" y1="-125.73" x2="38.1" y2="-134.62" width="0.1524" layer="91"/>
<junction x="38.1" y="-125.73"/>
<wire x1="38.1" y1="-100.33" x2="38.1" y2="-74.93" width="0.1524" layer="91"/>
<wire x1="38.1" y1="-74.93" x2="31.75" y2="-74.93" width="0.1524" layer="91"/>
<junction x="38.1" y="-100.33"/>
<pinref part="U$6" gate="G$1" pin="PIN-2"/>
<wire x1="31.75" y1="-74.93" x2="31.75" y2="-71.12" width="0.1524" layer="91"/>
<wire x1="38.1" y1="-74.93" x2="38.1" y2="-49.53" width="0.1524" layer="91"/>
<wire x1="38.1" y1="-49.53" x2="31.75" y2="-49.53" width="0.1524" layer="91"/>
<junction x="38.1" y="-74.93"/>
<pinref part="U$2" gate="G$1" pin="PIN-2"/>
<wire x1="31.75" y1="-49.53" x2="31.75" y2="-45.72" width="0.1524" layer="91"/>
<label x="38.1" y="-134.62" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
</net>
<net name="MX_COLUMN1" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="PIN-2"/>
<wire x1="8.89" y1="-45.72" x2="8.89" y2="-49.53" width="0.1524" layer="91"/>
<wire x1="8.89" y1="-49.53" x2="15.24" y2="-49.53" width="0.1524" layer="91"/>
<wire x1="15.24" y1="-49.53" x2="15.24" y2="-74.93" width="0.1524" layer="91"/>
<wire x1="15.24" y1="-74.93" x2="8.89" y2="-74.93" width="0.1524" layer="91"/>
<pinref part="U$5" gate="G$1" pin="PIN-2"/>
<wire x1="8.89" y1="-74.93" x2="8.89" y2="-71.12" width="0.1524" layer="91"/>
<wire x1="15.24" y1="-74.93" x2="15.24" y2="-100.33" width="0.1524" layer="91"/>
<wire x1="15.24" y1="-100.33" x2="8.89" y2="-100.33" width="0.1524" layer="91"/>
<junction x="15.24" y="-74.93"/>
<pinref part="U$9" gate="G$1" pin="PIN-2"/>
<wire x1="8.89" y1="-100.33" x2="8.89" y2="-96.52" width="0.1524" layer="91"/>
<wire x1="15.24" y1="-100.33" x2="15.24" y2="-125.73" width="0.1524" layer="91"/>
<wire x1="15.24" y1="-125.73" x2="8.89" y2="-125.73" width="0.1524" layer="91"/>
<junction x="15.24" y="-100.33"/>
<pinref part="U$13" gate="G$1" pin="PIN-2"/>
<wire x1="8.89" y1="-125.73" x2="8.89" y2="-121.92" width="0.1524" layer="91"/>
<wire x1="15.24" y1="-125.73" x2="15.24" y2="-134.62" width="0.1524" layer="91"/>
<junction x="15.24" y="-125.73"/>
<label x="15.24" y="-134.62" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
